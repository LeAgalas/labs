#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "lab_b.h"


int main() {

    printf("##################     LAB №0.5b     #################\n\n");

    char str[MAX_STRING_SIZE];
    strcpy(str, "");

    printf("Type your string: ");

    while(scanf("%[^\n]", str) != EOF){
        scanf("%*c");
        if (strlen(str)){

            List* lst;
            list_init(&lst, str[0]);
            //printf("%c ", lst->symbol);
            //1234.0002000 21.12400         4221.000123000        124.01240

            List* temp = lst;

            for (int i = 1; i < strlen(str); i++){
                char cur = str[i];
                if (isspace(cur)){
                    cur = ' ';
                }
                temp = list_add(temp, cur);
            }

            printf("Your list is: ");
            list_print(lst);

            remove_double_space(lst);
            printf("Removed double spaces: ");
            list_print(lst);

            lst = list_reverse_words(lst);
            printf("Swap of words: ");
            list_print(lst);

            lst = remove_double_zero(lst);
            printf("Removed double zeros: ");
            list_print(lst);

            printf("\n");

            lst = list_delete(lst);
            strcpy(str, "");
            //list_print(lst);

            printf("######################################################\n\n");
            printf("Type your next string: ");
        } else {
            printf("\nType again: ");
            scanf("%[^\n]", str);
        }
    }

    printf("\n\n[#] Exited after EOF\n");

    return 0;
}
