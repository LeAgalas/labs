#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "lab_b.h"

// List* list_init(char data){
//     List *lst;
//
//     lst = (List *)malloc(sizeof(List));
//     lst->symbol = data;
//     lst->next = NULL;
//
//     return lst;
// }


void list_init(List** lst, char data){

    *lst = (List *)malloc(sizeof(List*));
    (*lst)->symbol = data;
    (*lst)->next = NULL;

    //printf("%c ", lst->symbol);

}

List* list_add(List* lst, char data){
    List *temp,*ptr;
    temp = (List *)malloc(sizeof(List));

    ptr = lst->next; //saving last pointer
    lst->next = temp;
    temp->symbol = data;
    temp->next = ptr;

    return temp;
}

void list_print(List* lst){
    List* temp = lst;

    do {
        //printf("%c->", temp->symbol);
        printf("%c", temp->symbol);
        temp = temp->next;
    } while (temp != NULL);

    printf("->NULL\n");
}

void list_delete_item(List* head, List* elem){
    List* temp = head;

    while (temp->next != elem){
        temp = temp->next;
    }

    temp->next = elem->next;
    free(elem);
}

void remove_double_space(List* lst){
    List* temp = lst;

    while (temp->next != NULL){
        if (isspace(temp->symbol) && isspace(temp->next->symbol)){
            list_delete_item(lst, temp->next);
        } else {
            temp = temp->next;
        }
    }
}

List* list_delete_head(List* head){
    List* temp = head->next;
    free(head);
    return temp;
}

List* list_delete(List* lst){
    while (lst->next != NULL){
        lst = list_delete_head(lst);
    }
    lst = list_delete_head(lst);
    return lst;
}

List* swap(List* head, List* elem1, List* elem2){
    List *prev1 = head, *prev2 = head;
    List *cont1 = elem1->next, *cont2 = elem2->next;

    if (prev1 == elem1){
        prev1 = NULL;
    } else {
        while (prev1->next != elem1){ //find1
            prev1 = prev1->next;
        }
    }

    if (prev2 == elem2){
        prev2 = NULL;
    } else {
        while (prev2->next != elem2){ //find prev2
            prev2 = prev2->next;
        }
    }

    if (elem2 == cont1){
        elem2->next = elem1;
        elem1->next = cont2; // elem2 after elem1
        if (elem1 != head)
            prev1->next = elem2;

    } else if (elem1 == cont2){
        elem1->next = elem2;
        elem2->next = cont1;       //elem1 after elem2
        if (elem2 != head)
            prev2->next = elem1;

    } else {

        if (elem1 != head)
            prev1->next = elem2;
        if (elem2 != head)
            prev2->next = elem1;
        elem2->next = cont1;
        elem1->next = cont2;
    }

    if (elem1 == head)
        return elem2;
    if (elem2 == head)
        return elem1;

    return head;

}

List* list_reverse_words(List* head){
    List *space1 = head, *prev_space2 = head, *temp;
    int count, now, times;
    while (prev_space2->next != NULL) {
        count = 1;
        temp = space1;
        while (temp->next != NULL && temp->next->symbol != ' '){
            temp = temp->next;
            count++;
        }
        prev_space2 = space1;
        temp = space1;
        times = count/2;
        while(times > 0){
            now = 0;
            while (now < count-1){
                temp = temp->next;
                now++;
            }
            head = swap(head, space1, temp);

            count -= 2;
            times -= 1;
            space1 = temp;
            space1 = space1->next;
            temp = space1;
        }
        if (prev_space2->next != NULL){
            space1 = prev_space2->next->next;
        }
    }
    return head;

}

List* remove_double_zero(List* lst){
    List* temp = lst;
    int flag = 1;

    while (temp->next != NULL){
        if (flag && temp->symbol == '0' && temp->next->symbol == '0'){
            list_delete_item(lst, temp->next);
        } else {

            flag = 0;
            if (isspace(temp->symbol)){
                flag = 1;
            }
            if (temp->next != NULL)
                temp = temp->next;
        }
    } // double zeros

    temp = lst;
    flag = 1;

    while (temp->next != NULL){

        if (flag && temp->symbol == '0' && temp->next->symbol != '.'){
            if (lst != temp){
                list_delete_item(lst, temp);
                temp = lst;
            } else {
                lst = list_delete_head(lst);
                temp = lst;
            }
        } else {

            flag = 0;
            if (isspace(temp->symbol)){
                flag = 1;
            }
            if (temp->next != NULL)
                temp = temp->next;
        }
    }//forward zero
    return lst;
}
