#ifndef LAB_B_H
#define LAB_B_H

#define MAX_STRING_SIZE 1024

typedef struct List{
    char symbol;
    struct List* next;
} List;

//List* list_init(char data);
void list_init(List** lst, char data);
List* list_add(List* lst, char data);
void list_print(List* lst);
void list_delete_item(List* head, List* elem);
void remove_double_space(List* lst);
List* list_delete_head(List* head);
List* list_delete(List* lst);
List* swap(List* head, List* elem1, List* elem2);
List* list_reverse_words(List* head);
List* remove_double_zero(List* lst);


#endif
