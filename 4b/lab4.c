#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "lab4.h"


void table_init(Item hash_table[]){
    for (int i = 0; i < N; i++){
        hash_table[i].key = -1;
        hash_table[i].node = NULL;
    }
}


void add_element(Node** root, Item hash_table[]){
    int key, hashed, current;
    char *str = NULL;

    printf("Enter the key: ");
    get_int(&key);
    printf("Enter the data: ");
    scanf("%m[^\n]%*c", &str);


    hashed = hash(key);
    current = hashed;



    if (hash_table[current].node != NULL && hash_table[current].key == key){
        printf("[!] Rewriting existing node\n\n");
        hash_table[current].node->data = realloc(hash_table[current].node->data,
        sizeof(char)*(sizeof(str)+1));
        strcpy(hash_table[current].node->data, str);
    } else {
        printf("[+] Item has just added\n\n");
        hash_table[current].key = key;
        hash_table[current].node = insert(root, key, str);
    }
    for (int i = 0; i < N; i++)
        printf("[%d]: (%d)\n", i, hash_table[i].key);

}


int hash(int key){
    return key % N;
}


void insert_from_cmd(Node** root){
    char *str = NULL;
    int key;
    printf("Enter the key: ");
    get_int(&key);
    printf("Enter the data: ");
    scanf("%m[^\n]%*c", &str);
    insert(root, key, str);
    //insert(root, key, "kek");
}

void LeftRotate(Node** root, Node* node){

    if (!node || !node->right_child)
        return;

    Node* tmp_right = node->right_child;

    node->right_child = tmp_right->left_child;
    if (node->right_child != NULL){
        node->right_child->parent = node;
    }

    tmp_right->parent = node->parent;

    if (node->parent == NULL){
        (*root) = tmp_right;
    } else if (node == node->parent->left_child){
        node->parent->left_child = tmp_right;
    } else {
        node->parent->right_child = tmp_right;
    }

    tmp_right->left_child = node;

    node->parent = tmp_right;
}

void RightRotate(Node** root, Node* node){

    if (!node || !node->left_child)
        return;

    Node* tmp_left = node->left_child;

    node->left_child = tmp_left->right_child;
    if (tmp_left->right_child != NULL){
        tmp_left->right_child->parent = node;
    }

    tmp_left->parent = node->parent;

    if (tmp_left->parent == NULL){
        (*root) = tmp_left;
    } else if (node == node->parent->left_child){
        node->parent->left_child = tmp_left;
    } else {
        node->parent->right_child = tmp_left;
    }

    tmp_left->right_child = node;

    node->parent = tmp_left;
}

void insertFixUp(Node** root, Node* inserted){
    //printf("starting Balancing: %d", inserted->key);
    while (inserted != *root && inserted->parent->colour &&
        inserted != (*root)->right_child && inserted != (*root)->left_child) { // while red
        Node* uncle;
        //printf("Balancing: %d", inserted->key);
        //Finding uncle (other child of parent)
        if (inserted->parent && inserted->parent->parent &&
            inserted->parent == inserted->parent->parent->left_child){
            uncle = inserted->parent->parent->right_child;
        } else {
            uncle = inserted->parent->parent->left_child;
        }

        // If uncle is RED, do following
        // (i)  Change colour of parent and uncle as BLACK
        // (ii) Change colour of grandparent as RED
        // (iii) Move inserted to grandparent
        if (uncle && uncle->colour){
            uncle->colour = 0;
            inserted->parent->colour = 0;
            inserted->parent->parent->colour = 1;
            inserted = inserted->parent->parent;
        }
         // Uncle is BLACK, there are four cases (LL, LR, RL and RR)
        else {
            // Left-Left (LL) case, do following
            // (i)  Swap colour of parent and grandparent
            // (ii) Right Rotate Grandparent

            if (inserted->parent && inserted->parent->parent &&
                inserted->parent == inserted->parent->parent->left_child &&
                inserted->parent->left_child == inserted){
                    // colour Swap parent & grandpa
                    int colourtmp = inserted->parent->colour;
                    inserted->parent->colour = inserted->parent->parent->colour;
                    inserted->parent->parent->colour = colourtmp;
                    RightRotate(root, inserted->parent->parent);
            }
            // Left-Right (LR) case, do following
            // (i)  Swap colour of current node  and grandparent
            // (ii) Left Rotate Parent
            // (iii) Right Rotate Grand Parent
            if (inserted->parent && inserted->parent->parent &&
                inserted->parent == inserted->parent->parent->left_child &&
                inserted->parent->right_child == inserted){
                    // colour Swap parent & grandpa
                    int colourtmp = inserted->colour;
                    inserted->colour = inserted->parent->parent->colour;
                    inserted->parent->parent->colour = colourtmp;
                    LeftRotate(root, inserted->parent);
                    RightRotate(root, inserted->parent->parent);
            }
            // Right-Right (RR) case, do following
            // (i)  Swap colour of parent and grandparent
            // (ii) Left Rotate Grandparent
            if (inserted->parent && inserted->parent->parent &&
                inserted->parent == inserted->parent->parent->right_child &&
                inserted->parent->right_child == inserted){
                    // colour Swap parent & grandpa
                    int colourtmp = inserted->parent->colour;
                    inserted->parent->colour = inserted->parent->parent->colour;
                    inserted->parent->parent->colour = colourtmp;
                    LeftRotate(root, inserted->parent->parent);
            }
            // Right-Left (RL) case, do following
            // (i)  Swap colour of current node  and grandparent
            // (ii) Right Rotate Parent
            // (iii) Left Rotate Grand Parent
            if (inserted->parent && inserted->parent->parent &&
                inserted->parent == inserted->parent->parent->right_child &&
                inserted->parent->left_child == inserted){
                    // colour Swap parent & grandpa
                    int colourtmp = inserted->colour;
                    inserted->colour = inserted->parent->parent->colour;
                    inserted->parent->parent->colour = colourtmp;
                    RightRotate(root, inserted->parent);
                    LeftRotate(root, inserted->parent->parent);
            }

        }
        (*root)->colour = 0; //black
    }
}

Node* insert(Node** root, int key, char* data){

    Node* temp = (Node*)malloc(sizeof(Node));
    temp->key = key;
    temp->data = data;
    temp->parent = NULL;
    temp->left_child = NULL;
    temp->right_child = NULL;

    if (*root == NULL){
        temp->colour = 0;
        (*root) = temp;
        //printf("[+] New root has just inserted\n\n");
    }
    else {
        Node* current = (*root);
        Node* tmp_par = NULL;
        int l_or_r = 0; // l = 0 r = 1
        while (current != NULL){
            tmp_par = current;
            if (key == current->key){
                free(temp);
                //printf("[!] Node with such key exists\n\n");
                return NULL;
            } else if (key < current->key){
                current = current->left_child;
                l_or_r = 0;
            } else {
                current = current->right_child;
                l_or_r = 1;
            }
        }
        temp->parent = tmp_par;
        if (l_or_r) {
            tmp_par->right_child = temp;
        } else {
            tmp_par->left_child = temp;
        }
        temp->colour = 1; //red
        //printf("[+] New node has just inserted\n\n");
        // fix tree
        insertFixUp(root,temp);
    }
    return temp;
}

Node* search(Node** root, int key){
    Node* found = NULL;
    Node* cur = (*root);
    while(cur){
        if (cur->key == key){
            found = cur;
            cur = NULL;
        }
        else if (key < cur->key){
            cur = cur->left_child;
        } else {
            cur = cur->right_child;
        }
    }
    return found;
}

void transplant(Node** root, Node* u, Node* v){
    if(!u->parent){
        if (v)
            v->parent = NULL;
        *root = v;
        return;
    } else if (u == u->parent->left_child)
        u->parent->left_child = v;
    else {
        u->parent->right_child = v;
    }
    if (v)
        v->parent = u->parent;
}

Node* minimum(Node* x){
    while(x->left_child){
        x = x->left_child;
    }
    return x;
}

Node* maximum(Node* x){
    while(x->right_child){
        x = x->right_child;
    }
    return x;
}

void delete_fixup(Node** root, Node* x){
    while(x && x != *root && !x->colour) {//black
        if(x == x->parent->left_child){
            Node* w = x->parent->right_child;
            if(w->colour){
                w->colour = 0;
                x->parent->colour = 1;
                LeftRotate(root, x->parent);
                w = x->parent->right_child;
            }
            if(!w->left_child->colour && !w->right_child->colour){
                w->colour = 1;
                x = x->parent;
            }
            else {
                if(!w->right_child->colour){
                    w->left_child->colour = 0;
                    w->colour = 1;
                    RightRotate(root, w);
                    w = x->parent->right_child;
                }
                w->colour = x->parent->colour;
                x->parent->colour = 0;
                w->right_child->colour = 0;
                LeftRotate(root, x->parent);
                x = *root;
            }
        }
        else {
            Node* w = x->parent->left_child;
            if (w->colour){
                w->colour = 0;
                x->parent->colour = 1;
                RightRotate(root, x->parent);
                w = x->parent->left_child;
            }
            if (!w->right_child->colour && !w->left_child->colour){
                w->colour = 1;
                x = x->parent;
            }
            else {
                if (!w->left_child->colour){
                    w->right_child->colour = 0;
                    w->colour = 1;
                    LeftRotate(root, w);
                    w = x->parent->left_child;
                }
                w->colour = x->parent->colour;
                x->parent->colour = 0;
                w->left_child->colour = 0;
                RightRotate(root, x->parent);
                x = (*root);
            }
        }
    }

    if (x)
        x->colour = 0;
}

void delete(Node** root, Node* z, Item cache[]){
    Node* y = z;
    Node* x;


    int y_original = y->colour;


    // delete node and right subtree to that place
    if (!z->left_child){

        x = z->right_child;
        transplant(root, z, z->right_child);
    } else if (!z->right_child){
        // delete node and left subtree to that place
        x = z->left_child;
        transplant(root, z, z->left_child);
    } else {
        y = minimum(z->right_child);
        y_original = y->colour;
        x = y->right_child;
        // if (y->parent == z){
        //     if (x){
        //         x->parent = z;
        //         x->
        //     }
        // }
        if (y->parent != z){
            transplant(root, y, y->right_child);
            y->right_child = z->right_child;
            y->right_child->parent = y;
        }
        transplant(root, z, y);
        y->left_child = z->left_child;
        y->left_child->parent = y;
        y->colour = z->colour;
    }
    if (!y_original) //black
        delete_fixup(root, x);
    cache[hash(z->key)].node = NULL;
    cache[hash(z->key)].key = -1;
    // free(z);
    // free(z->data);
    if (*root){
        (*root)->parent = NULL;
    }
}

void print_tree(Node* node){
    if (node == NULL){
        return;
    }else{
        print_tree(node->left_child);
        print_node(node);
        print_tree(node->right_child);
    }
}

void print_node(Node *node){
    if (node == NULL){
        printf("[!] Node is empty\n\n");
    }
    else {
        printf("Current node:\n");
        printf("-- Key is %d\n", node->key);
        printf("-- Colour is %d\n", node->colour);
        printf("-- Data is %s\n", node->data);
        if (node->parent != NULL)
            printf("-- Parent key is %d\n", node->parent->key);
        if (node->left_child != NULL)
            printf("-- Left key is %d\n", node->left_child->key);
        if (node->right_child != NULL)
            printf("-- Right key is %d\n", node->right_child->key);
    }
    printf("\n");
}

void to_file(Node* node, FILE* ptr){
    if (node == NULL){
        return;
    }else{
        to_file(node->left_child, ptr);

        fprintf(ptr, "%d\n", node->key);
        fprintf(ptr, "%s\n", node->data);

        to_file(node->right_child, ptr);
        free(node->data);
        free(node);
    }
}

int get_int(int* a){
    int n;
    do{
        n = scanf("%d", a);
        if (n == 0){
            printf("\nTry to enter number again: ");
        }
        scanf("%*[^\n]");
        scanf("%*c");
    } while(n == 0);
    return n < 0 ? 0 : 1;
}

void rbtree_to_file(Node** root, char* filename){
    FILE* ptr = fopen(filename,"w");

    to_file(*root, ptr);

    fclose(ptr);
}

void file_to_rbtree(Node** root,char* filename){
    FILE* ptr = fopen(filename,"r");
    int tmp_key;
    char* tmp_data;

    while (fscanf(ptr, "%d", &tmp_key) != EOF){
        fscanf(ptr, "%ms", &tmp_data);
        printf("[%] Reading: [%d] %s\n", tmp_key, tmp_data);
        insert(root, tmp_key, tmp_data);
    }
    fclose(ptr);
}

int D_Timing(Node **root)
{
    int n = 10, key[10000], k, cnt = 100000, i, m;
    clock_t first, last;
    srand(time(NULL));
    while(n--> 0){
    for (i = 0; i < 10000; ++i)
        key[i] = rand() * rand() % (100*cnt);
    for (i = 0; i < cnt; ){
        k = rand() * rand() % (100*cnt);
        if (insert(root, k, "meme"))
            ++i;
        //printf("%d\n\n", i);
    }
    m = 0;
    first = clock();
    for (i = 0; i < 10000; ++i){
        if (search(root, key[i]))
            ++m;
    }
    last = clock();
    printf("%d items was found\n", m);
    //double diff;
    //diff = last-first;
    printf("test #%d, number of nodes = %d, time = %.3f\n", 10, (10 -
n)*cnt, (double)(last-first)/CLOCKS_PER_SEC);
    }
    return 1;
}

void print_menu(int* option){
    printf("[1] Insert element into RB-Tree table\n");
    printf("[2] Delete element from RB-Tree table\n");
    printf("[3] Search element in RB-Tree table\n");
    printf("[4] Print RB-Tree\n");
    printf("[5] Print Max and Min in RB-Tree\n");
    printf("[6] Exit\n");
    printf("[7] Timing/Profiling\n");

    printf("Choose an option [1-7]: ");
    get_int(option);

    printf("\n");
}
rbtree_to_file(&root, filename);
            free(filename);
            printf("[!] Exited by user\n");
