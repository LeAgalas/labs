#ifndef LAB4_H
#define LAB4_H

#define N 5

typedef struct Node{
    int colour; // 1 = red; 0 = black
    int key;
    char* data;
    struct Node* parent;
    struct Node* left_child;
    struct Node* right_child;
} Node;

typedef struct Item{
    int key;
    Node* node;
} Item;

//RB work funcs
Node* insert(Node** root, int key, char* data);
void insert_from_cmd(Node** root);
void LeftRotate(Node** root, Node* node);
void RightRotate(Node** root, Node* node);
void insertFixUp(Node** root, Node* inserted);
void delete(Node** root, Node* z, Item cache[]);
Node* search(Node** root, int key);
Node* maximum(Node* x);
Node* minimum(Node* x);

//helper funcs
int D_Timing(Node **root);
int hash(int key);
void add_element(Node** root, Item hash_table[]);
void rbtree_to_file(Node** root, char* filename);
void file_to_rbtree(Node** root, char* filename);
void print_node(Node* node);
void print_tree(Node* node);
void print_menu(int* option);
int get_int(int* a);

#endif
