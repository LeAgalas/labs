#ifndef LAB0_H
#define LAB0_H

int get_int(int* a);
int* get_array(int* array, int n);
void print_array(int* array, int n);
int swap_numerals(int* num);
void swap_numerals_array(int* array, int n);
void swap(int* x, int* y);
void bubbleSort(int* arr, int n);
int get_last(int x);

#endif
