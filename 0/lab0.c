#include <stdio.h>
#include <stdlib.h>
#include "lab0.h"

int get_int(int* a){
    int n;
    do{
        n = scanf("%d", a);
        if (n == 0){
            printf("Try to enter number again\n");
            scanf("%*s");
        }
    } while(n == 0);
    return n < 0 ? 0 : 1;
}

int* get_array(int* array, int n){
    array = (int* )malloc(sizeof(int)*n);
    if (array == NULL){
        printf("Malloc of size %d failed!\n", n);
        exit(1);
    }
    int a;
    for (int i = 0; i < n; i++){
        printf("Enter number #%d: ", i+1);
        get_int(&a);
        array[i] = a;
    }
    return array;
}

void print_array(int* array, int n){
    for (int i = 0; i < n; i++){
        printf("%d ", array[i]);
    }
    printf("\n");
}

int get_last(int x){
    if (x < 0){
        x *= -1;
    }
    return x % 10;
}

int swap_numerals(int* num){
    int new_num = 0, sign = 0, cur = *num;
    int ost1, ost2, counter=1;

    if (cur < 0){
        sign = 1;
        cur *= -1;
    }

    while (cur > 0) {
        ost1 = cur % 10;
        ost2 = (cur % 100 - ost1)/10;
        if (ost1 == ost2){
            new_num += (10 * ost1 + ost2) * counter;
        }
        else {
            cur /= 100;
            new_num += (10 * ost1 + ost2) * counter;
            counter *= 100;
        ]
    }

    *num = new_num;
    if (sign){
        *num *= -1;
    }

    return get_last(new_num);
}

void swap_numerals_array(int* array, int n){
    for (int i = 0; i < n; i++){
        swap_numerals(&array[i]);
    }
}

void swap(int* x, int* y){
    int temp = *x;
    *x = *y;
    *y = temp;
}

void bubbleSort(int* arr, int n){
    int i, j;
    for (i = 0; i < n-1; i++){
        for (j = 0; j < n - i - 1; j++){
            if (get_last(arr[j]) < get_last(arr[j+1]))
                swap(&arr[j], &arr[j+1]);
        }
    }
}
