#include <stdio.h>
#include <stdlib.h>
#include "lab0.h"

int main(){

    int a, n;
    int* array;

    // Get the n
    printf("Enter amount of numbers (n): ");
    get_int(&n);

    // Get the array
    array = get_array(array, n);
    printf("Your array is: ");
    print_array(array, n);

    // Swapping numerals
    swap_numerals_array(array, n);
    printf("Array with swapped numerals: ");
    print_array(array, n);

    //Sorting
    bubbleSort(array, n);
    printf("Sorted by last numeral in descending order: ");
    print_array(array, n);


    //Don`t forget to free memory
    free(array);
    
    return 0;
}
