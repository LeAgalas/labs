#ifndef LAB1_H
#define LAB1_H


typedef struct Lines{
    int length;
    int* elements;
    double average;
} Lines;

int get_int(int* a);
void find_average_vector(Lines* matrix, int m, int* answer);
void print_answer(int* answer, int m);
int* get_array(int* array, int n);
void print_array(int* array, int n);
Lines* get_matrix(Lines* matrix, int m);
void print_matrix(Lines* matrix, int m);
Lines* find_average(Lines* matrix, int m);
void freeMemory(Lines* matrix, int m);


#endif
