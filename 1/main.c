#include <stdio.h>
#include <stdlib.h>
#include "lab1.h"

int main(){

    // Matrix m x ni
    int m;
    Lines* matrix = NULL;


    printf("###################     LAB №1     ###################\n\n");

    printf("Enter number of rows (m): ");
    get_int(&m);
    printf("\n");

    printf("######################################################\n\n");

    matrix = get_matrix(matrix, m);
    matrix = find_average(matrix, m);

    printf("######################################################\n\n");

    print_matrix(matrix, m);

    printf("######################################################\n\n");

    int answer[m];
    find_average_vector(matrix, m, answer);
    print_answer(answer, m);

    //free memory
    freeMemory(matrix, m);

    return 0;
}
