#include <stdio.h>
#include <stdlib.h>
#include "lab1.h"


int get_int(int* a){
    int n;
    do{
        n = scanf("%d", a);
        if (n == 0){
            printf("Try to enter number again\n");
            scanf("%*s");
        }
    } while(n == 0);
    return n < 0 ? 0 : 1;
}

void print_array(int* array, int n){
    for (int i = 0; i < n; i++){
        printf("%d ", array[i]);
    }
}

int* get_array(int* array, int n){
    //array = (int* )malloc(sizeof(int)*n);
    array = (int* )calloc(n, sizeof(int));
    if (array == NULL){
        printf("Malloc of size %d failed!\n", n);
        exit(1);
    }
    int a;

    for (int i = 0; i < n; i++){
        get_int(&a);
        array[i] = a;
    }
    return array;
}

Lines* get_matrix(Lines* matrix, int m){
    matrix = (Lines* )malloc(sizeof(Lines)*m);
    if (matrix == NULL){
        printf("Malloc of size %d failed!\n", m);
        exit(1);
    }
    for (int i = 0; i < m; i++){

        printf("Enter length of row #%d: ", i+1);
        get_int(&matrix[i].length);

        printf("Enter row #%d: ", i+1);
        matrix[i].elements = get_array(matrix[i].elements, matrix[i].length);

        printf("\n");
    }
    return matrix;
}

Lines* find_average(Lines* matrix, int m){
    for (int i = 0; i < m; i++){
        matrix[i].average = 0;
        for (int j = 0; j < matrix[i].length; j++){
            matrix[i].average += matrix[i].elements[j];
        }
        matrix[i].average /=  matrix[i].length;
    }
    return matrix;
}

void find_average_vector(Lines* matrix, int m, int* answer){
    int temp;
    float cur;
    for (int i = 0; i < m; i++){
        temp = 0;
        for (int j = 0; j < matrix[i].length; j++){
            cur = matrix[i].elements[j];
            if ((float)cur > matrix[i].average)
                temp += cur;
        }
        answer[i] = temp;
    }
}

void print_answer(int* answer, int m){
    printf("The answer is: ");
    for (int i = 0; i < m; i++){
        printf("%d ", answer[i]);
    }
    printf("\n\n");
}

void print_matrix(Lines* matrix, int m){
    for (int i = 0; i < m; i++){

        print_array(matrix[i].elements, matrix[i].length);
        printf("\n");
    }
    printf("\n");
    for (int i = 0; i < m; i++){
        printf("Row #%d: ", i+1);
        printf("Length is %d; ", matrix[i].length);
        printf("Average is %.3f\n", matrix[i].average);
        printf("\n");
    }
}

void freeMemory(Lines* matrix, int m){
    for (int i = 0; i < m; i++)
    {
        free(matrix[i].elements);
    }
    free(matrix);
}
