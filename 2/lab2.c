#include <stdio.h>
#include <string.h>
#include "lab2.h"

int get_int(int* a){
    int n;
    do{
        n = scanf("%d", a);
        if (n == 0){
            printf("Try to enter number again\n");
        }
        scanf("%*[^\n]");
        scanf("%*c");
    } while(n == 0);
    return n < 0 ? 0 : 1;
}

void parse_input_data(char words[3][MAX_RECORD_SIZE]){
    char current_record[MAX_RECORD_SIZE];
    char delim[] = "/";

    printf("\nType your string: ");
    scanf("%[^\n]", current_record);
    scanf("%*c");

    char *ptr = strtok(current_record, delim);

    int j = 0;

     while (ptr != NULL){
        strcpy(words[j++], ptr);
        ptr = strtok(NULL, delim);
    }
}
