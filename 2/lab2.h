#ifndef LAB2_H
#define LAB2_H

#define MAX_RECORD_SIZE 1024


typedef struct Passenger{
    char* id;
    unsigned int ta;
    unsigned int tl;
} Passenger;

int get_int(int* a);
void parse_input_data(char words[3][MAX_RECORD_SIZE]);

#endif
