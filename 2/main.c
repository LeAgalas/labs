#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lab2.h"


int main(){

    printf("###################     LAB №2     ###################\n\n");

    int checkin_count;

    printf("Enter amount of checkin counters: ");
    get_int(&checkin_count);

    for (int i = 0; i < checkin_count; i++){

        char words[3][MAX_RECORD_SIZE];

        parse_input_data(words);

        //printf("Current string is: %s\n", current_record);
        printf("Passenger data is: %s %d %d\n", words[0], atoi(words[1]), atoi(words[2]));
    }



    return 0;
}
