#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "lab4.h"


int main() {

    printf("##################      LAB №4      ##################\n\n");

    int menu_option, key;
    Item cache[N];
    Node *root = NULL;

    char* filename;

    printf("Enter logs name: ");
    scanf("%m[^\n]%*c", &filename);

    if (access(filename, F_OK ) == 0 ){
       file_to_rbtree(&root, filename);
    }

    do {
        print_menu(&menu_option);

        switch(menu_option){

        case 1:
            //insert_from_cmd(&root, cache);
            add_element(&root, cache);
            break;
        case 2:
            printf("Enter a key: ");
            get_int(&key);
            Node* searched_del = search(&root, key);

            if (searched_del){
                //printf("Found is %d %d", searched_del, &root);
                delete(&root, searched_del, cache);
                printf("[+] Item deleted\n\n");
            }else
                printf("[-] Node with such key hasn`t found\n\n");
            break;
        case 3:
            printf("Enter a key: ");
            get_int(&key);
            Node* searched = search(&root, key);
            if (searched)
                printf("[+] Item found\n\n");
            else
                printf("[-] Node with such key hasn`t found\n\n");
            break;
        case 4:
            //print
            if (root == NULL){
                printf("[!] Tree is empty\n\n");
            } else {
                print_tree(root);
            }
            break;
        case 5:
            if (root == NULL){
                printf("[!] Tree is empty\n\n");
            } else {
                Node* max_el = maximum(root);
                Node* min_el = minimum(root);
                printf("Maximum is [%d] %s | Minimum is [%d] %s\n\n",
                max_el->key, max_el->data, min_el->key, min_el->data);
            }
            break;
        case 6:
            //Exit + freeing
            rbtree_to_file(&root, filename);
            free(filename);
            printf("[!] Exited by user\n");
            //free_memory(hash_table);
            break;
        case 7:
            D_Timing(&root);
            free(filename);
            break;
        default:
            printf("[!] No such option!\n\n");
        }

    } while (menu_option < 6 );
    return 0;
}
