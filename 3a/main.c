#include <stdio.h>
#include "lab3.h"


int main() {

    printf("##################      LAB №3      ##################\n\n");

    int menu_option, searched;
    Item hash_table[SIZE];
    table_init(hash_table);

    if (!check_consts()){
        do {
            print_menu(&menu_option);

            switch(menu_option){

            case 1:
                add_element(hash_table);
                break;
            case 2:
                delete_element(hash_table);
                break;
            case 3:
                searched = search(hash_table);
                if (searched != -1) {
                    printf("\nFound [%d]: (%d, %d, %s)\n\n", searched,
                    hash_table[searched].key1, hash_table[searched].key2,
                    hash_table[searched].info);
                }
                break;
            case 4:
                print_hash_table(hash_table);
                break;
            case 5:
                printf("[!] Exited by user\n");
                free_memory(hash_table);
                break;
            default:
                printf("[!] No such option!\n\n");
            }

        } while (menu_option != 5);
    }
    else {
        printf("[!] GCD of STEP and SIZE must be equal to one\n\n");
    }
    return 0;
}
