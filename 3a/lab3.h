#ifndef LAB3_H
#define LAB3_H


#define SIZE 5
#define STEP 4
#define MAX_STRING_SIZE 1024


typedef struct Item{
    int key1;
    int key2;
    char *info;
} Item;

void print_menu(int* option);
int get_int(int* a);
void string_init(char** string_ptr);
void string_free(char** string_ptr);
void set_string(char** destination);
int check_consts();
int gcd(int a, int b);
int hash(int key);

void table_init(Item hash_table[]);
void add_element(Item hash_table[]);
int search_key1(Item hash_table[], int key);
int search_key2(Item hash_table[], int key);
void delete_element(Item hash_table[]);
void print_hash_table(Item hash_table[]);
int search(Item hash_table[]);
void free_memory(Item hash_table[]);

#endif
