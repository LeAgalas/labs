#ifndef STRASSEN_H
#define STRASSEN_H

#define LIMIT 100


int** initializeMatrix(int n);
void zeroMatrix(int** A, int n);
void randMatrix(int** A, int n);
void printMatrix(int** A, int n);
void freeMatrix(int** A, int n);

int** matrixMultiply(int** A, int** B, int n);
int** subMatrix(int** A, int** B, int n);
int** addMatrix(int** A, int** B, int n);
int** strassenMultiply(int** A, int** B, int n);

#endif
