#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "strassen.h"


int main(){

    srand(time(0));
    int n = 750;
    clock_t first, last;

    int **A = initializeMatrix(n);
    int **B = initializeMatrix(n);
    int **C;
    int** D;

    randMatrix(A, n);
    printf("First Matrix is generated\n");
    //printMatrix(A, n);

    randMatrix(B, n);
    printf("Second Matrix is generated\n\n");
    //printMatrix(B, n);


    first = clock();
    C =  matrixMultiply(A, B, n);
    last = clock();
    printf("A*B Matrix is calculated\n");

    //printMatrix(C, n);
    printf("Total time taken to multiply %dx%d matrices: %.5f\n\n", n, n,
        (double)(last - first)/CLOCKS_PER_SEC);

    first = clock();
    D =  strassenMultiply(A, B, n);
    last = clock();
    printf("A*B Matrix is calculated with Strassen method\n");

    //printMatrix(C, n);
    printf("Total time taken to multiply %dx%d matrices with Strassen: %.5f\n\n",
        n, n, (double)(last - first)/CLOCKS_PER_SEC);

    freeMatrix(A, n);
    freeMatrix(B, n);
    freeMatrix(C, n);
    freeMatrix(D, n);
    return 0;
}
