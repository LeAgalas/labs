#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "strassen.h"


int** initializeMatrix(int n){
    int** temp;
    temp = (int**)malloc(n*sizeof(int*));
    for (int i = 0; i < n; i++){
        temp[i] = (int*)malloc(n*sizeof(int));
    }
    return temp;
}

void randMatrix(int** A, int n){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            A[i][j] = rand() % LIMIT;
        }
    }
}

void zeroMatrix(int** A, int n){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            A[i][j] = 0;
        }
    }
}

void printMatrix(int** A, int n){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            printf("%d ", A[i][j]);
        }
        printf("\n");
    }
}

int** matrixMultiply(int** A, int** B, int n){
    int ** C = initializeMatrix(n);
    zeroMatrix(C, n);
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            for(int k = 0; k < n; k++)
                C[i][j] += A[i][k] * B[k][j];
    return C;
}

int** addMatrix(int** A, int** B, int n) {
    int** temp = initializeMatrix(n);
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            temp[i][j] = A[i][j] + B[i][j];
    return temp;
}

int** subMatrix(int** A, int** B, int n) {
    int** temp = initializeMatrix(n);
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            temp[i][j] = A[i][j] - B[i][j];
    return temp;
}

int** strassenMultiply(int** A, int** B, int n){
    if (n <= 70) {
        int** C = matrixMultiply(A, B, n);
        return C;
    }

    int** C = initializeMatrix(n);
    int k = n/2;
    int** A11 = initializeMatrix(k);
    int** A12 = initializeMatrix(k);
    int** A21 = initializeMatrix(k);
    int** A22 = initializeMatrix(k);
    int** B11 = initializeMatrix(k);
    int** B12 = initializeMatrix(k);
    int** B21 = initializeMatrix(k);
    int** B22 = initializeMatrix(k);
    for(int i=0; i<k; i++){
        for(int j=0; j<k; j++) {
            A11[i][j] = A[i][j];
            A12[i][j] = A[i][k+j];
            A21[i][j] = A[k+i][j];
            A22[i][j] = A[k+i][k+j];
            B11[i][j] = B[i][j];
            B12[i][j] = B[i][k+j];
            B21[i][j] = B[k+i][j];
            B22[i][j] = B[k+i][k+j];
        }
    }
    int** tmp;
    int** tmp2;

    tmp = subMatrix(B12, B22, k);
    int** P1 = strassenMultiply(A11, tmp, k);
    freeMatrix(tmp, k);

    tmp = addMatrix(A11, A12, k);
    int** P2 = strassenMultiply(tmp, B22, k);
    freeMatrix(tmp, k);

    tmp = addMatrix(A21, A22, k);
    int** P3 = strassenMultiply(tmp, B11, k);
    freeMatrix(tmp, k);

    tmp = subMatrix(B21, B11, k);
    int** P4 = strassenMultiply(A22, tmp, k);
    freeMatrix(tmp, k);

    tmp = addMatrix(A11, A22, k);
    tmp2 = addMatrix(B11, B22, k);
    int** P5 = strassenMultiply(tmp, tmp2, k);
    freeMatrix(tmp, k);
    freeMatrix(tmp2, k);

    tmp = subMatrix(A12, A22, k);
    tmp2 = addMatrix(B21, B22, k);
    int** P6 = strassenMultiply(tmp, tmp2, k);
    freeMatrix(tmp, k);
    freeMatrix(tmp2, k);

    tmp = subMatrix(A11, A21, k);
    tmp2 = addMatrix(B11, B12, k);
    int** P7 = strassenMultiply(tmp, tmp2, k);
    freeMatrix(tmp, k);
    freeMatrix(tmp2, k);

    tmp = addMatrix(P5, P4, k);
    tmp2 = addMatrix(tmp, P6, k);
    int** C11 = subMatrix(tmp2, P2, k);
    freeMatrix(tmp, k);
    freeMatrix(tmp2, k);

    int** C12 = addMatrix(P1, P2, k);
    int** C21 = addMatrix(P3, P4, k);

    tmp = addMatrix(P5, P1, k);
    tmp2 = subMatrix(tmp, P3, k);
    int** C22 = subMatrix(tmp2, P7, k);
    freeMatrix(tmp, k);
    freeMatrix(tmp2, k);

    for(int i=0; i<k; i++){
        for(int j=0; j<k; j++) {
            C[i][j] = C11[i][j];
            C[i][j+k] = C12[i][j];
            C[k+i][j] = C21[i][j];
            C[k+i][k+j] = C22[i][j];
        }
    }
    freeMatrix(A11, k);
	freeMatrix(A12, k);
	freeMatrix(A21, k);
	freeMatrix(A22, k);
	freeMatrix(B11, k);
	freeMatrix(B12, k);
	freeMatrix(B21, k);
	freeMatrix(B22, k);
	freeMatrix(P1, k);
	freeMatrix(P2, k);
	freeMatrix(P3, k);
	freeMatrix(P4, k);
	freeMatrix(P5, k);
	freeMatrix(P6, k);
	freeMatrix(P7, k);
	freeMatrix(C11, k);
	freeMatrix(C12, k);
	freeMatrix(C21, k);
	freeMatrix(C22, k);

    return C;

}


void freeMatrix(int** A, int n){
    for (int i = 0; i < n; i++){
        free(A[i]);
    }
    free(A);
}
