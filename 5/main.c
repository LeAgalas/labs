#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lab5.h"


int main() {

    printf("##################      LAB №5      ##################\n\n");

    //0 make graph struct
    Graph *g = createGraph();
    double x, y;
    char *name, *name1, *name2;
    Vertex *find1, *find2;

    int V = 10, key;
    char* names[V];
    srand(time(NULL));
    for (int i = 0; i < V; i++){
        key = rand() % (10000*V);
        char* str;
        str = malloc(sizeof(char)*10);
        sprintf(str, "%d", key);
        names[i] = str;
    }
    int m = V*V/2;
    m = rand()%m;

    for(int i=0; i<V; i++){
        printf("%s ", names[i]);
        addVertex(g, rand()%10000, rand()%10000, names[i]);
    }
    for(int i=0; i<m; i++){
        int first, second;
        first = rand()%V;
        second = rand()%V;
        while (second == first)
            second = rand()%V;

        find1 = findVertex(g, names[first]);
        find2 = findVertex(g, names[second]);
        addEdge(find1, find2);
    }
    printf("\n\n");


    // addVertex(g, 3.0, 4.0, "lol");
    // addVertex(g, 4.0, 5.0, "kek");
    // addVertex(g, -3.0, -4.0, "mem");
    // addVertex(g, 10.0, 0.0, "deb");
    // addVertex(g, 8.0, 6.0, "ken");
    // addVertex(g, 5.0, 5.0, "flex");
    //
    // find1 = findVertex(g, "kek");
    // find2 = findVertex(g, "lol");
    // addEdge(find1, find2);
    //
    // find1 = findVertex(g, "kek");
    // find2 = findVertex(g, "flex");
    // addEdge(find1, find2);
    //
    // find1 = findVertex(g, "lol");
    // find2 = findVertex(g, "flex");
    // addEdge(find1, find2);
    //
    // find1 = findVertex(g, "ken");
    // find2 = findVertex(g, "lol");
    // addEdge(find1, find2);
    //
    // find1 = findVertex(g, "deb");
    // find2 = findVertex(g, "lol");
    // addEdge(find1, find2);
    //
    // find1 = findVertex(g, "mem");
    // find2 = findVertex(g, "kek");
    // addEdge(find1, find2);
    //
    //
    // find1 = findVertex(g, "flex");
    // find2 = findVertex(g, "mem");
    // addEdge(find1, find2);
    //
    // find1 = findVertex(g, "flex");
    // find2 = findVertex(g, "kek");
    // addEdge(find1, find2);
    //
    // find1 = findVertex(g, "deb");
    // find2 = findVertex(g, "ken");
    // addEdge(find1, find2);
    //
    // find1 = findVertex(g, "deb");
    // find2 = findVertex(g, "flex");
    // addEdge(find1, find2);
    //1 vertice adding
    int menu_option;

    do {
        print_menu(&menu_option);

        switch(menu_option){

        case 1:
            //Adding vertex

            printf("Enter x coordinate: ");
            get_double(&x);
            printf("Enter y coordinate: ");
            get_double(&y);
            printf("Enter name of vertex: ");
            scanf("%m[^\n]%*c", &name);
            if(findVertex(g, name)){
                printf("[!] Vertex with such name exists\n");
            }
            else{
                addVertex(g, x, y, name);
                printf("[+] Vertex has successfully added\n");
            }
            printf("\n");
            break;
        case 2:
            //adding edge
            printf("Enter name of the first vertex: ");
            scanf("%m[^\n]%*c", &name1);
            printf("Enter name of the second vertex: ");
            scanf("%m[^\n]%*c", &name2);
            find1 = findVertex(g, name1);
            if (!find1){
                printf("[!] There is no the first vertex\n");
                break;
            }
            find2 = findVertex(g, name2);
            if (!find2){
                printf("[!] There is no the second vertex\n");
                break;
            }
            if (find1 == find2){
                printf("[!] There are the same vertexes\n");
                break;
            }
            addEdge(find1, find2);
            printf("[+] Edge has successfully added\n");
            break;
        case 3:
            //Deleting vertex
            printf("Enter name of the vertex to delete: ");
            scanf("%m[^\n]%*c", &name);
            find1 = findVertex(g, name);
            if (!find1){
                printf("[!] There is no vertex with such name\n\n");
            } else {
                //
                deleteAllEdges(g, find1);
                deleteVertex(g, find1);
                printf("[+] Deleted successfully\n\n");
            }
            break;
        case 4:
            //Printing
            printGraph(g);
            break;
        case 5:
            //Dijkstra
            printf("Enter name of the vertex to dijkstra: ");
            scanf("%m[^\n]%*c", &name);
            find1 = findVertex(g, name);
            printf("\n");
            if (!find1){
                printf("[!] There is no vertex with such name\n\n");
            } else {
                dijkstra(g, find1);
            }

            break;
        case 6:
            printf("[!] Exited by user\n");
            //Freeing
            //free_memory(hash_table);
            //free(filename);
            break;
        default:
            printf("[!] No such option!\n\n");
        }

    } while (menu_option != 6);
    return 0;
}
