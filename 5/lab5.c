#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "lab5.h"


// typedef struct Vertex{
//     double x;
//     double y;
//     char *name;
//     struct Vertex* next;
//     struct Edge *edge;
// }Vertex;
//
// typedef struct Edge{
//     double weight;
//     struct Vertex vertex;
//     struct Edge *next;
// }Edge;
//
// typedef struct Graph{
//     int Vcount; //Vertices count
//     //may be edge count?
//     Vertex *adjLists;
// }Graph;

Graph* createGraph(){
    Graph *graph = malloc(sizeof(Graph));
    graph->adjLists = NULL;
    graph->Vcount = 0;
    return graph;
}

void addEdge(Vertex* from, Vertex* to){
    Edge* newEdge = malloc(sizeof(Edge));
    newEdge->vertex = to;
    newEdge->next = NULL;
    newEdge->weight = distance(from, to);
    if (from->edge){
        Edge* tmp = from->edge;
        while(tmp->next != NULL){
            if (strcmp(tmp->vertex->name, to->name) == 0 ){
                printf("[!] This edge exists");
                return;
            }
            tmp = tmp->next;
        }
        tmp->next = newEdge;
    } else {
        from->edge = newEdge;
    }
}

void deleteAllEdges(Graph* g, Vertex* x){
    Vertex* temp_v = g->adjLists;
    while (temp_v){

        Edge* temp_e = temp_v->edge;
        if (temp_e && temp_e->vertex == x){
            temp_v->edge = temp_e->next;
            temp_v = temp_v->next;
            //free
            continue;
        }
        while (temp_e && temp_e->next){
            if (temp_e->next->vertex == x){
                temp_e->next = temp_e->next->next;
                //free
                break;
            }
            temp_e = temp_e->next;
        }
        temp_v = temp_v->next;
    }
}

void deleteVertex(Graph* g, Vertex* x){
    Vertex* temp_v = g->adjLists;
    if (temp_v == x){
        g->adjLists = temp_v->next;
        free(temp_v);
    } else {
        while (temp_v->next != x){
            temp_v = temp_v->next;
        }
        temp_v->next = temp_v->next->next;
        while (temp_v){
            temp_v = temp_v->next;
            if (temp_v)
                temp_v->num--;
        }
        //free(temp_v->next);
    }
    g->Vcount -= 1;
}

void addVertex(Graph* g, double x, double y, char* name) {
    Vertex *v = malloc(sizeof(Vertex));
    v->x = x;
    v->y = y;
    v->name = name;
    v->num = g->Vcount;
    v->next = NULL;
    if (g->adjLists){
        Vertex* tmp = g->adjLists;
        while(tmp->next != NULL){
            tmp = tmp->next;
        }
        tmp->next = v;
    } else {
        g->adjLists = v;
    }

    g->Vcount+=1;
}

int min_Dist(double dist[], int visited[], int n)
//This method used to find the vertex with minimum distance and is not yet visited
{
	int index;
    double min = 100000;           //Initialize min with infinity
	for(int v=0;v<n;v++)
	{
		if(!visited[v] && dist[v]<=min)
		{
			min=dist[v];
			index=v;
		}
	}
	return index;
}

void dijkstra(Graph *g, Vertex* start){
    int n = g->Vcount;
    int start_num = start->num;
    int visited[n];
    double cost[n][n];
    double dist[n];

    for(int i = 0; i < n; i++){
        dist[i] = INF;
        visited[i] = 0;
    }
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            cost[i][j] = INF;
        }
    }

    Vertex* temp_v = g->adjLists;
    while (temp_v){
        Edge* temp_e = temp_v->edge;
        while (temp_e != NULL){
            cost[temp_v->num][temp_e->vertex->num] =  temp_e->weight;
            temp_e = temp_e->next;
        }

        temp_v = temp_v->next;
    }

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (cost[i][j] != INF)
                printf("%lf ", cost[i][j]);
            else
                printf("Inf ");
        }
        printf("\n");
    }
    printf("\n");
    dist[start_num] = 0;

    for(int c = 0; c < n-1; c++){
        int u = min_Dist(dist, visited, n);
        visited[u] = 1;
        for (int v = 0; v<n; v++){
            if(!visited[v] && cost[u][v] && dist[u]+cost[u][v] < dist[v]){
                dist[v] = dist[u]+cost[u][v];
            }
        }
    }

    printf("From %s\n", start->name);
    temp_v = g->adjLists;
    for(int i = 0; i < n; i++){
        if (dist[i] == INF){
            printf("No way to %s\n", temp_v->name);
        } else if (i != start_num){
            printf("Distance to %s is %lf\n", temp_v->name, dist[i]);
        }
        temp_v = temp_v->next;
    }
    printf("\n");
}

void printGraph(Graph* g){
    Vertex* temp_v = g->adjLists;
    if (!temp_v){
        printf("Graph is empty :(\n\n");
    } else {
        while (temp_v){
            printf("[%s(%.2f;%.2f)]: [ ", temp_v->name, temp_v->x, temp_v->y);
            Edge* temp_e = temp_v->edge;
            while (temp_e != NULL){
                printf("%s(%.2lf) ", temp_e->vertex->name, temp_e->weight);
                temp_e = temp_e->next;
            }
            printf("]\n");
            temp_v = temp_v->next;
        }
        printf("\n");
    }
}

Vertex* findVertex(Graph* g, char* name){
    Vertex* temp_v = g->adjLists;
    while (temp_v != NULL && strcmp(temp_v->name, name) != 0) {
        temp_v = temp_v->next;
    }
    return temp_v;
}

double distance(Vertex* u, Vertex* v){
    return sqrt((u->x-v->x)*(u->x-v->x)+(u->y-v->y)*(u->y-v->y));
}

int get_int(int* a){
    int n;
    do{
        n = scanf("%d", a);
        if (n == 0){
            printf("\nTry to enter number again: ");
        }
        scanf("%*[^\n]");
        scanf("%*c");
    } while(n == 0);
    return n < 0 ? 0 : 1;
}

int get_double(double* a){
    int n;
    do{
        n = scanf("%lf", a);
        if (n == 0){
            printf("\nTry to enter number again: ");
        }
        scanf("%*[^\n]");
        scanf("%*c");
    } while(n == 0);
    return n < 0 ? 0 : 1;
}

void print_menu(int* option){
    printf("[1] Add Vertex to Graph\n");
    printf("[2] Add Edge to Graph\n");
    printf("[3] Delete Vertex\n");
    printf("[4] Print Graph\n");
    printf("[5] Find way (Dijkstra)\n");
    printf("[6] Exit\n");

    printf("Choose an option [1-6]: ");
    get_int(option);

    printf("\n");
}
