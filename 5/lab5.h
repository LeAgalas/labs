#ifndef LAB5_H
#define LAB5_H

#define INF 100000


typedef struct Vertex{
    double x;
    double y;
    char *name;
    int num;
    struct Vertex* next;
    struct Edge *edge;
}Vertex;

typedef struct Edge{
    double weight;
    struct Vertex* vertex;
    struct Edge *next;
}Edge;

typedef struct Graph{
    int Vcount;
    //may be edge count?
    Vertex *adjLists;
}Graph;

void dijkstra(Graph *g, Vertex* start);
void deleteAllEdges(Graph* g, Vertex* x);
char* my_itoa(int number);
void deleteVertex(Graph* g, Vertex* x);
void addEdge(Vertex* from, Vertex* to);
Vertex* findVertex(Graph* g, char* name);
Graph* createGraph();
void printGraph(Graph* g);
void addVertex(Graph* g, double x, double y, char* name);
double distance(Vertex* u, Vertex* v);
int get_int(int* a);
int get_double(double* a);
void print_menu(int* option);

#endif
