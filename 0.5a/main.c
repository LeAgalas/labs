#include <stdio.h>
#include "lab0_5.h"


int main() {

    printf("##################     LAB №0.5     ##################\n\n");

    char str[MAX_STRING_SIZE];

    printf("Type your string: ");
    // This is the t  h  e      test stri   ng    ,  .   this is how ; its works .
    while(scanf("%[^\n]", str) != EOF){
        scanf("%*c");

        remove_double_space(str);
        remove_leading_space(str);
        //parse_string(str);

        printf("Redacted string is: %s\n\n", str);

        printf("######################################################\n\n");
        printf("Type your next string: ");
    }

    printf("\n\n[#] Exited after EOF\n");

    return 0;
}
