#ifndef LAB0_5_H
#define LAB0_5_H

#define MAX_STRING_SIZE 1024

void parse_string(char* str);
void remove_leading_space(char* str);
void remove_double_space(char* str);
void replace_char(char* str, char find, char replace);
void reverse_string(char* str);

#endif
