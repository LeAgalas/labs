#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "lab0_5.h"


void remove_double_space(char* str){
    int i = 0, x = 0;
    for(; str[i]; ++i){
        // isspace is working for all white space chars
        //  ' ' - space, '\t' -  hor tab, '\n' - newline, '\v' - vert tab
        //  '\f' - form feed, '\r' - carraige return
        //clearing double space and double tabs
        if (!isspace(str[i]) || (i > 0 && !isspace(str[i-1])))
            str[x++] = str[i];
    }
    str[x] = '\0';
}

void remove_leading_space(char* str){
    char punctuations[] = ",;.!?";
    int i = 0, x = 0;
    for(; str[i]; ++i){
        if(!isspace(str[i]) ||  strchr(punctuations, str[i+1]) == NULL) {
             str[x++] = str[i];
        }
    }
    str[x] = '\0';
}

void reverse_string(char* str){
    char ans[MAX_STRING_SIZE]="", ans2[MAX_STRING_SIZE]="";

    int len = strlen(str), j = 0;

    for (int i = 0; i < len; i++){
        ans[i] = str[len-i-1];
    }

    while (ans[j] == '0')
        j++;

    if (ans[j] == '.')
        j--;

    for (int i = 0; j < len; i++, j++){
        ans2[i] = ans[j];
    }

    strcpy(str, ans2);
}

void replace_char(char* str, char find, char replace){
    char *current_pos = strchr(str,find);
    while (current_pos) {
        *current_pos = replace;
        current_pos = strchr(current_pos,find);
    }
}

void parse_string(char* str){
    int init_size = strlen(str);
	char delim[] = " ";
    replace_char(str,'\t', ' ');

	char *ptr = strtok(str, delim);
    char words[MAX_STRING_SIZE][MAX_STRING_SIZE/2];
    int wcount = 0;

    char ans[MAX_STRING_SIZE] = "";

	while(ptr != NULL)
	{
		//printf("'%s'\n", ptr);
        strcpy(words[wcount++], ptr);
		ptr = strtok(NULL, delim);
	}

	//
	for (int i = 0; i < wcount; i++)
	{
		//printf("%s ", words[i]);
        reverse_string(words[i]);
        //printf("%s \n", words[i]);

        strncat(ans, words[i], strlen(words[i]));
        strncat(ans, delim, strlen(delim));
	}
	//printf("\nAns is: %s\n", ans);
    strcpy(str, ans);
}
