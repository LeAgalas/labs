#ifndef LLRB_H
#define LLRB_H

typedef struct Node{
  int key;
  int value;
  int color; // 1 is red and 0 is black
  struct Node *left;
  struct Node *right;
} Node;

//llrb funcs
Node* RightRotate(Node* node);
Node* LeftRotate(Node* node);
void colorSwap(Node* node);

Node* moveRedRight(Node* node);
Node* moveRedLeft(Node* node);
Node* balance(Node* node);
Node* findMin(Node* node);
void insert(int key, int value);
Node* insertNode(Node* node, int key, int value);
Node* makeNode(int key, int value);

Node* removeMin(Node* node);
Node* removeNode(Node* node, int key);
Node* removeKey(Node* node, int key);
Node* clearLLRB(Node* node);
Node* search(Node* node, int key);

//helpers
int get_int(int* a);
void print_menu(int* option);
void print_node(Node *node);
void print_tree(Node *node);

#endif
