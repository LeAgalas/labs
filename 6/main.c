#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "llrb.h"


int main() {

    printf("#################      LAB LLRB      #################\n\n");

    int menu_option, key, value;
    extern Node *root;
    // char* filename;
    //
    // printf("Enter logs name: ");
    // scanf("%m[^\n]%*c", &filename);
    //
    // if (access(filename, F_OK ) == 0 ){
    //    file_to_rbtree(&root, filename);
    // }

    do {
        print_menu(&menu_option);

        switch(menu_option){

        case 1:
            printf("Enter a key: ");
            get_int(&key);
            //printf("Enter a value: ");
            //get_int(&value);
            insert(key, key);
            break;
        case 2:
            printf("Enter a key: ");
            get_int(&key);
            root = removeKey(root, key);
            // Node* searched_del = search(&root, key);
            //
            // if (searched_del){
            //     //printf("Found is %d %d", searched_del, &root);
            //     delete(&root, searched_del, cache);
            //     printf("[+] Item deleted\n\n");
            // }else
            //     printf("[-] Node with such key hasn`t found\n\n");
            break;
        case 3:
            printf("Enter a key: ");
            get_int(&key);
            Node* searched = search(root, key);
            if (searched)
                printf("[+] Item found: %d\n\n",searched->value);
            else
                printf("[-] Node with such key hasn`t found\n\n");
            break;
        case 4:
            //print
            print_tree(root);
            break;
        case 5:
            //Exit + freeing
            //rbtree_to_file(&root, filename);
            //free(filename);
            printf("[!] Exited by user\n");
            clearLLRB(root);
            break;
        default:
            printf("[!] No such option!\n\n");
        }

    } while (menu_option != 5 );
    return 0;
}
