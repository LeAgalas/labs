#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "llrb.h"

Node* root = (Node*)NULL;

Node* removeMin(Node* node){
    if (!node)
        return (Node*)NULL;
    if (!node->left){
        free(node);
        return (Node*)NULL;
    }
    if ((!node->left || !node->left->color) && (!node->left->left || !node->left->left->color))
        node = moveRedLeft(node);
    node->left = removeMin(node->left);
    return balance(node);
}

Node* removeNode(Node* node, int key){
    Node* tmp;
    if (!node)
        return (Node*)NULL;
    if (key < node->key){
        if (node->left){
            if (!node->left->color && (!node->left-> left || !node->left->left->color))
                node = moveRedLeft(node);
            node->left = removeKey(node->left, key);
        }
    } else {
        if (node->left && node->left->color)
            node = RightRotate(node);
        if (key <= node->key && !node->right){
            free(node);
            return (Node*)NULL;
        }
        if (node->right){
            if (node->right->color && (!node->right->left || !node->right->left->color))
    	       node = moveRedRight(node);
            if (key <= node->key){
    	           tmp = findMin(node->right);
    	           node->key = tmp->key;
    	           node->value = tmp->value;
    	           node->right = removeMin(node->right);
            }
            else
    	       node->right = removeKey(node->right, key);
        }
    }
    return balance(node);
}

Node* removeKey(Node* node, int key){
    node = removeNode(node, key);
    if (node)
        node->color = 0;
    return node;
}

Node* clearLLRB(Node* node){
    if (node) {
        if (node->left)
            clearLLRB(node->left);
        if (node->right)
            clearLLRB(node->right);
       node->left = (Node*)NULL;
       node->right = (Node*)NULL;
       free(node);
    }
    return (Node*)NULL;
}

Node* makeNode(int key, int value){
    Node* tmp;
    tmp = (Node*)malloc(sizeof(*tmp));
    tmp->key = key;
    tmp->value = value;
    tmp->color = 1;
    tmp->left = (Node*)0;
    tmp->right = (Node*)0;
    return tmp;
}

Node* insertNode(Node* node, int key, int value){
    if (!node)
        return makeNode(key, value);

    if (key == node->key){
        node->value = value;
    } else if (key < node->key) {
        node->left = insertNode(node->left, key, value);
    } else {
        node->right = insertNode(node->right, key, value);
    }
    if (node->right && node->right->color && (!node->left || !node->left->color))
        node = LeftRotate(node);
    if (node->left && node->left->color && node->left->left && node->left->left->color)
        node = RightRotate(node);
    if (node->left && node->left->color && node->right && node->right->color)
        colorSwap(node);
    return node;
}

void insert(int key, int value){
    root = insertNode(root, key, value);
    if (root)
        root->color = 0;
}

Node* findMin(Node* node){
    if (!node)
        return (Node *)NULL;
    while (node->left)
        node = node->left;
    return node;
}

Node* balance(Node* node){
    if (node->left && node->left->color)
        node = LeftRotate(node);
    if (node->left && node->left->color && node->left->left && node->left->left->color)
        node = RightRotate(node);
    if (node->left && node->left->color && node->right && node->right->color)
        colorSwap(node);
    return node;
}

Node* moveRedLeft(Node* node){
    colorSwap(node);
    if (node && node->right && node->right->left && node->right->left->color){
        node->right = RightRotate(node->right);
        node = LeftRotate(node);
        colorSwap(node);
    }
    return node;
}

Node* moveRedRight(Node* node){
    colorSwap(node);
    if (node && node->left && node->left->left && node->left->left->color){
        node = RightRotate(node);
        colorSwap(node);
    }
    return node;
}

Node* search(Node* node, int key){
    Node* found = NULL;
    Node* cur = root;
    while(cur){
        if (cur->key == key){
            found = cur;
            cur = NULL;
        }
        else if (key < cur->key){
            cur = cur->left;
        } else {
            cur = cur->right;
        }
    }
    return found;
}

void colorSwap(Node* node){
    node->color = !(node->color);
    if (node->left)
        node->left->color = !(node->left->color);
    if (node->right)
        node->right->color = !(node->right->color);
}

Node* LeftRotate(Node* node){
    if (!node){
        return (Node*)node;
    }
    Node* right = node->right;
    node->right = right->left;
    right->left = node;
    right->color = node->color;
    node->color = 1; // red
    return right;
}

Node* RightRotate(Node* node){
    if (!node){
        return (Node*)node;
    }
    Node* left = node->left;
    node->left = left->right;
    left->right = node;
    left->color = node->color;
    node->color = 1; // red
    return left;
}

void print_tree(Node* node){
    if (node == NULL){
        return;
    }else{
        print_tree(node->left);
        print_node(node);
        print_tree(node->right);
    }
}

void print_node(Node *node){
    if (node == NULL){
        printf("[!] Node is empty\n\n");
    }
    else {
        printf("Current node:\n");
        printf("-- Key is %d\n", node->key);
        printf("-- Colour is %d\n", node->color);
        printf("-- Value is %d\n", node->value);
        if (node->left != NULL)
            printf("-- Left key is %d\n", node->left->key);
        if (node->right != NULL)
            printf("-- Right key is %d\n", node->right->key);
    }
    printf("\n");
}

int get_int(int* a){
    int n;
    do{
        n = scanf("%d", a);
        if (n == 0){
            printf("\nTry to enter number again: ");
        }
        scanf("%*[^\n]");
        scanf("%*c");
    } while(n == 0);
    return n < 0 ? 0 : 1;
}

void print_menu(int* option){
    printf("[1] Insert element into LLRB-Tree table\n");
    printf("[2] Delete element from LLRB-Tree table\n");
    printf("[3] Search element in LLRB-Tree table\n");
    printf("[4] Print LLRB-Tree\n");
    printf("[5] Exit\n");

    printf("Choose an option [1-5]: ");
    get_int(option);

    printf("\n");
}
