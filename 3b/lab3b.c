#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lab3b.h"

int get_int(int* a){
    int n;
    do{
        n = scanf("%d", a);
        if (n == 0){
            printf("\nTry to enter number again: ");
        }
        scanf("%*[^\n]");
        scanf("%*c");
    } while(n == 0);
    return n < 0 ? 0 : 1;
}

int gcd(int a, int b) {
    if (b != 0)
        return gcd(b, a % b);
    else
        return a;
}

int hash(int key){
    return key % SIZE;
}

int check_consts(){
    return gcd(STEP, SIZE) != 1;
}

void string_init(char** string_ptr) {
  char *ptr = malloc(16);
  *string_ptr = ptr;
}

void hash_table_to_file(Item hash_table[], char* filename){
    FILE* ptr = fopen(filename,"wb");
    for (int i = 0; i < SIZE; i++) {
        if (hash_table[i].info != NULL){
            char* serialized;
            int size = 3 * sizeof(int) + strlen(hash_table[i].info);
            serialized = malloc(sizeof(char) * (size));
            serialize(hash_table[i], serialized);
            //deserialize(hash_table[i], serialized);
            //printf("\nSer-d %d", *((int*)(serialized + sizeof(int))));
            fwrite(serialized, size, 1, ptr);

            free(serialized);
        }
    }

    fclose(ptr);
}

void file_to_hash_table(Item hash_table[], char* filename){
    FILE* ptr = fopen(filename,"rb");
    int saved;
    int key1, key2, info_len, hashed, find1, find2, current, i = 0, number=1, off=0;
    //fread(&info_len, sizeof(int), 1, ptr);

    while (fread(&info_len, sizeof(int), 1, ptr)) {
        fread(&key1, sizeof(int), 1, ptr);
        fread(&key2, sizeof(int), 1, ptr);
        char* info;
        info = malloc(info_len * sizeof(char));

        //printf("%d", saved);
        saved = ftell(ptr);
        //printf("%d", saved);
        fread(info, sizeof(char), info_len, ptr);

        //printf("\nReaded %d %d %d %s", info_len, key1, key2, info);

        hashed = hash(key2);
        current = hashed;
        find1 = search_key1(hash_table, key1);
        find2 = search_key2(hash_table, key2);

        do {
            if (hash_table[current].info == NULL){
                if (find1 != -1 && hash_table[find1].info != NULL) {
                    printf("[!] There are element with the same the first key\n\n");
                    return;
                } else if (find2 != -1 && hash_table[find2].info != NULL) {
                    printf("[!] There are element with the same the second key\n\n");
                    return;
                } else if (hash_table[current].key1 != -1){
                    hash_table[current].key1 = key1;
                    hash_table[current].key2 = key2;
                    hash_table[current].info_len = info_len;
                    printf("%d", saved);
                    hash_table[current].offset = saved;

                    printf("[+] Table load: Item #%d has just updated\n", number++);
                    break;
                }
            } else if (hash_table[current].key2 == key2){
                printf("[!] There are element with the same the second key\n\n");
                return;
            }
            i += STEP;
            current = (hashed + i) % SIZE;
        } while ( current != hashed );
        //printf("[!] Hash table is full\n\n");


    }
    printf("[+] Hash table is up to date\n\n");
    fclose(ptr);
}



void serialize(Item element, char* serialized){
    int info_len = strlen(element.info), k1 = element.key1, k2 = element.key2;
    // Saving two keys and info size
    *((int*)serialized) = info_len;
    *((int*)(serialized + sizeof(int))) = k1;
    *((int*)(serialized + 2*sizeof(int))) = k2;
    strncpy(serialized + 3*sizeof(int), element.info, info_len);
    //printf("\nSER in func: %d", *((int*)(serialized + sizeof(int))));
}


void* deserialize(Item element, char* serialized){
    int info_len;
    info_len = *((int*)serialized);
    element.key1 = *((int*)(serialized  + sizeof(int)));
    element.key2 = *((int*)(serialized + 2*sizeof(int)));
    element.info = malloc(sizeof(char) * info_len);

    strncpy(element.info, serialized + 3 * sizeof(int), info_len);
    //printf("Deserialized element is: %d %d %s", element.key1, element.key2,
    //                        element.info);
}

void set_string(char** destination) {
    char value[MAX_STRING_SIZE];
    scanf("%1024s", value);
    int new_size = strlen(value);
    *destination = realloc(*destination, sizeof(char)*new_size + 1);

    if(*destination == NULL) {
      printf("Unable to realloc() ptr!");
    } else {
      strcpy(*destination, value);
    }
}

void table_init(Item hash_table[]){
    for (int i = 0; i < SIZE; i++){
        hash_table[i].info = NULL;
        hash_table[i].key1 = 0;
        hash_table[i].key2 = 0;
    }
}

void add_element(Item hash_table[]){
    int key1, key2, hashed, i = 0, current, find1, find2;
    char* info;

    string_init(&info);

    printf("Enter the first key: ");
    get_int(&key1);
    printf("Enter the second key: ");
    get_int(&key2);
    printf("Enter info: ");
    set_string(&info);

    hashed = hash(key2);
    current = hashed;
    find1 = search_key1(hash_table, key1);
    find2 = search_key2(hash_table, key2);
    do {
        if (hash_table[current].info == NULL){
            if (find1 != -1 && hash_table[find1].info != NULL) {
                printf("[!] There are element with the same the first key\n\n");
                return;
            } else if (find2 != -1 && hash_table[find2].info != NULL) {
                printf("[!] There are element with the same the second key\n\n");
                return;
            } else if (hash_table[current].key1 != -1){
                hash_table[current].key1 = key1;
                hash_table[current].key2 = key2;
                //hash_table[current].info = info;
                printf("[+] Item has just added\n\n");
                return;
            }
        } else if (hash_table[current].key2 == key2){
            printf("[!] There are element with the same the second key\n\n");
            return;
        }
        i += STEP;
        current = (hashed + i) % SIZE;
    } while ( current != hashed );
    printf("[!] Hash table is full\n\n");
}
int search(Item hash_table[]){
    int found = -1, key, key_dimension;
    printf("Enter the key: ");
    get_int(&key);
    printf("Enter dimension of the key: ");
    get_int(&key_dimension);

    if (key_dimension == 1)
        found = search_key1(hash_table, key);
    else if (key_dimension == 2)
        found = search_key2(hash_table, key);
    else if (key_dimension != 2  && key_dimension != 1){
        printf("[!] Key dimension must equal to 1 or to 2\n\n");
    }
    if (found == -1){
        printf("[!] No item with such key\n\n");
    }
    return found;
}

void delete_element(Item hash_table[]){
    int found;

    found = search(hash_table);

    if (found != -1){
        free(hash_table[found].info);
        hash_table[found].info  = NULL;
        hash_table[found].key1 = 0;
        hash_table[found].key2 = 0;
        printf("[+] Item has just deleted\n\n");
    }
}

void free_memory(Item hash_table[]){
    for (int i = 0; i < SIZE; i++){
        free(hash_table[i].info);
    }
    printf("[!] Memory freed\n\n");
}

int search_key1(Item hash_table[], int key){
    int found = -1;
    for (int i = 0; i < SIZE; i++){
        if (hash_table[i].key1 == key && hash_table[i].info != NULL){
            found = i;
            break;
        }
    }
    return found;
}

int search_key2(Item hash_table[], int key){
    int found = -1, hashed = hash(key), i = 0, current = hashed;
    do {
        if (hash_table[current].key2 == key && hash_table[current].info != NULL){
            found = current;
            break;
        }
        i += STEP;
        current = (hashed + i) % SIZE;
    } while ( current != hashed );
    return found;
}

void print_hash_table(Item hash_table[], char* filename){
    printf("Item format is (Key1, Key2, Info)\n");
    for (int i = 0; i < SIZE; i++){
        if (hash_table[i].info == NULL && hash_table[i].key1 == 0)
            printf("[%d]: Empty\n", i);
        else if (hash_table[i].info == NULL && hash_table[i].key1 == -1)
            printf("[%d]: Deleted\n", i);
        else{
            FILE* ptr = fopen(filename,"rb");
            char* info;
            info = malloc(hash_table[i].info_len * sizeof(char));
            int *read_to= malloc(hash_table[i].offset * sizeof(char));
;
            fread(read_to, sizeof(char), hash_table[i].offset, ptr);
            fread(info, sizeof(char), hash_table[i].info_len, ptr);
            printf("[%d]: (%d, %d, %s)\n", i, hash_table[i].key1, hash_table[i].key2, info);
            //printf("%d\n", ptr+3*sizeof(int));
            //printf("%d\n", hash_table[i].offset);

            //fread(hash_table[i].info, sizeof(char), hash_table[i].info_len, hash_table[i].offset);
            //printf("[%d]: (%d, %d, %s)\n", i, hash_table[i].key1, hash_table[i].key2, hash_table[i].info);
            free(info);
            free(read_to);
            fclose(ptr);
        }
    }

    printf("\n");
}

void print_menu(int* option){
    printf("[1] Add element to Hash table\n");
    printf("[2] Delete element from Hash table\n");
    printf("[3] Search element in Hash table\n");
    printf("[4] Print Hash table\n");
    printf("[5] Exit\n");

    printf("Choose an option [1-5]: ");
    get_int(option);

    printf("\n");
}
