#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "lab3b.h"


int main() {

    printf("##################      LAB №3      ##################\n\n");

    int menu_option, searched;
    Item hash_table[SIZE];
    table_init(hash_table);


    char* filename;
    FILE *file;
    string_init(&filename);
    printf("Enter logs name: ");
    set_string(&filename);
    // char* serialized;
    // serialized = malloc(sizeof(char) * (3 * sizeof(int) + strlen(hash_table[0].info)));
    // serialized = serialize(hash_table[0]);
    //
    // deserialize(hash_table[0], serialized);
    //printf("%s\n", serialized);
    if (access(filename, F_OK ) == 0 ){
       file_to_hash_table(hash_table, filename);
    }


    if (!check_consts()){
        do {
            print_menu(&menu_option);

            switch(menu_option){

            case 1:
                add_element(hash_table);
                hash_table_to_file(hash_table, filename);
                break;
            case 2:
                delete_element(hash_table);
                hash_table_to_file(hash_table, filename);
                break;
            case 3:
                searched = search(hash_table);
                if (searched != -1) {
                    printf("\nFound [%d]: (%d, %d, %s)\n\n", searched,
                    hash_table[searched].key1, hash_table[searched].key2,
                    hash_table[searched].info);
                }
                break;
            case 4:
                print_hash_table(hash_table, filename);
                break;
            case 5:
                printf("[!] Exited by user\n");
                free_memory(hash_table);
                free(filename);
                break;
            default:
                printf("[!] No such option!\n\n");
            }

        } while (menu_option != 5);
    }
    else {
        printf("[!] GCD of STEP and SIZE must be equal to one\n\n");
    }
    return 0;
}
